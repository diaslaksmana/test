import { useState } from 'react'
import './App.css'
import Logo from './assets/logo.png'
import Notif from './assets/notif.png'
import Message from './assets/message.png'
import Setting from './assets/settings.png'
import Avatar from './assets/avatar.png'
import Success from './assets/Vector.png'
import Cancel from './assets/cancel.png'
import Calender from './assets/calender.png'
import Profile from './assets/profile.png'
import Left from './assets/left.png'
import Home from './assets/home.png'
import Search from './assets/search.png'
import Outline from './assets/outline.png'
import Shcedule from './assets/schedule.png'

function App() {

  return (
    <div className="App">
      <header id='header' className=''>
        <nav className='container-fluid'>
          {/*  top bar*/}
          <div className='row top-bar pt-3'>
            <div className='col-12 col-sm-12 col-md-6 col-lg-6'>
              <div className='top-bar-left'>
                <ul>
                  <li>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-list" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                  </svg>
                  </li>
                  <li>
                    <img src={Logo}/>
                  </li>
                </ul>
              </div>
            </div>
            <div className='col-12 col-sm-12 col-md-6 col-lg-6'>
              <div className='top-bar-right'>
                <ul>
                  <li className='searchbox'>
                    <div className='search'>
                      <input type="email" className="form-control" id="" placeholder="Search"/>
                    </div>
                  </li>
                  <li>
                    <img src={Notif}/>
                  </li>
                  <li>
                    <img src={Message}/>
                  </li>
                  <li>
                    <img src={Setting}/>
                  </li>
                  <li>
                    <img src={Avatar}/>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {/* Top Menu */}
          <div className='top-menu'>
            <ul>
              <li>
                <a href=''>Home</a>
              </li>
              <li>
                <a href=''>Explore</a>
              </li>
              <li>
                <a href=''>Feed</a>
              </li>
              <li className='active'>
                <a href=''>Scheduled</a>
              </li>
            </ul>
          </div>
          <div className='mobile-top-header'>
            <a href=''>
              <img src={Left}/> Meets
            </a>
          </div>
        </nav>
      </header>

      {/* Section */}

      <section id='scheduled' className=' py-5'>
        <div className='container'>
          <div className='scroll-tabs'>
            <ul className="nav  nav-pills nav-fill my-3" id="pills-tab" role="tablist">
              <li className="nav-item" role="presentation">
                <button className="nav-link " id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
              </li>
              <li className="nav-item" role="presentation">
                <button className="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Explore <span>1</span></button>
              </li>
              <li className="nav-item" role="presentation">
                <button className="nav-link active" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Pending Meets <span>3</span></button>
              </li>
              <li className="nav-item" role="presentation">
                <button className="nav-link" id="pills-disabled-tab" data-bs-toggle="pill" data-bs-target="#pills-disabled" type="button" role="tab" aria-controls="pills-disabled" aria-selected="false" >Past Meets</button>
              </li>
            </ul>
          </div>
         
          <div className="tab-content" id="pills-tabContent">
            <div className="tab-pane fade   py-3" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" >
              
            </div>
            <div className="tab-pane fade py-3" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" >...</div>
            <div className="tab-pane fade show py-3 active" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab" >
              <h5>Pending Meets</h5>             
              <ul className='sort'>
                <li className='active'>
                  All
                </li>
                <li>
                  Accepted
                </li>
                <li>
                  Rejected
                </li>
                <li>
                  Reschedule
                </li>
              </ul>
              <div className='content'>
                <div className='row'>
                  <div className='col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4'>
                    <div className='card'>
                      <div className='card-header'>
                        <h5><span className='lable-success'><img src={Success}/></span> Huixian accepted the meet-up request</h5>
                      </div>
                      <div className='card-body'>
                        <div className='cover'>
                          <img src={Profile}/>
                        </div>
                        <div className='profile'>
                            <ul>
                              <li>
                                <label>Date</label> : 23 May 2022
                              </li>
                              <li>
                                <label>Time</label> : 7.00 pm - 10.00 pm
                              </li>
                              <li>
                                <label>Location</label> : Skai bar & lounge
                              </li>
                              <li>
                                <label>Activity</label> : Dinner
                              </li>
                              <li>
                                <label>Remarks</label> : Smart casual attire
                              </li>
                            </ul>
                        </div>
                      </div>
                      <div className='card-footer'>
                        <div className='button-top mb-3'>
                          <button type="button" className="btn btn-primary">Confirm and Pay</button>
                        </div>
                        <div className='button-bottom  '>
                          <div className='row'>
                            <div className='col-12 col-md-6'>
                              <button type="button" className="btn btn-otline-primary">Reschedule</button>
                            </div>
                            <div className='col-12 col-md-6'>
                              <button type="button" className="btn btn-otline-primary">Cancel Request</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4'>
                    <div className='card'>
                      <div className='card-header'>
                        <h5><span className='lable-success'><img src={Cancel}/></span> Chloe rejected the meet-up request</h5>
                      </div>
                      <div className='card-body'>
                        <div className='cover'>
                          <img src={Profile}/>
                        </div>
                        <div className='profile'>
                            <ul>
                              <li>
                                <label>Date</label> : 23 May 2022
                              </li>
                              <li>
                                <label>Time</label> : 7.00 pm - 10.00 pm
                              </li>
                              <li>
                                <label>Location</label> : Skai bar & lounge
                              </li>
                              <li>
                                <label>Activity</label> : Dinner
                              </li>
                              <li>
                                <label>Remarks</label> : Smart casual attire
                              </li>
                            </ul>
                        </div>
                      </div>
                      <div className='card-footer'>
                        <div className='button-top mb-3'>
                          <button type="button" className="btn btn-secondary">Ok</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className='col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4'>
                    <div className='card'>
                      <div className='card-header'>
                        <h5><span className='lable-success'><img src={Calender}/></span> Cassia rescheduled meet-up request</h5>
                      </div>
                      <div className='card-body'>
                        <div className='cover'>
                          <img src={Profile}/>
                        </div>
                        <div className='profile'>
                            <ul>
                              <li>
                                <label>Date</label> : 23 May 2022
                              </li>
                              <li>
                                <label>Time</label> : 7.00 pm - 10.00 pm
                              </li>
                              <li>
                                <label>Location</label> : Skai bar & lounge
                              </li>
                              <li>
                                <label>Activity</label> : Dinner
                              </li>
                              <li>
                                <label>Remarks</label> : Smart casual attire
                              </li>
                            </ul>
                        </div>
                      </div>
                      <div className='card-footer'>
                        <div className='button-top mb-3'>
                          <button type="button" className="btn btn-primary">Confirm and Pay</button>
                        </div>
                        <div className='button-bottom  '>
                          <div className='row'>
                            <div className='col-12 col-md-6'>
                              <button type="button" className="btn btn-otline-primary">Reschedule</button>
                            </div>
                            <div className='col-12 col-md-6'>
                              <button type="button" className="btn btn-otline-primary">Cancel Request</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div className="tab-pane fade py-3" id="pills-disabled" role="tabpanel" aria-labelledby="pills-disabled-tab" >...</div>
          </div>
        </div>
      </section>

      {/* Mobile menu */}
      <section id='moblile-menu'>
        <div className='containe-fluid'>
          <div className='tab-menu'>
            <ul>
              <li>
                <a href=''><img src={Home}/></a>
              </li>
              <li>
                <a href=''><img src={Search}/></a>
              </li>
              <li>
                <a href=''><img src={Outline}/></a>
              </li>
              <li>
                <a href=''><img src={Shcedule}/></a>
              </li>
            </ul>
          </div>
        </div>
      </section>

    </div>
  )
}

export default App
